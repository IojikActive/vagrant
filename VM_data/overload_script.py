import requests
from multiprocessing import Pool, TimeoutError, current_process


def worker(x):
    print("Worker name:%s",current_process().name)
    return x**x

if __name__ == "__main__":
    MAX = 40000

    with Pool(4) as pool:
        res = pool.map(worker,range(MAX))
        print(res)

