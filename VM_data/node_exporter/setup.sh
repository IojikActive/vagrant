#!/bin/bash

wget https://github.com/prometheus/node_exporter/releases/download/v1.3.1/node_exporter-1.3.1.linux-amd64.tar.gz
tar xvf node_exporter-1.3.1.linux-amd64.tar.gz
cd node_exporter-1.3.1.linux-amd64
#cp node_exporter /usr/local/bin/
cp node_exporter /usr/sbin/
cd .. 
rm -rf ./node_exporter-1.3.1.linux-amd64
rm -rf node_exporter-1.3.1.linux-amd64.tar.gz
useradd --no-create-home --shell /bin/false node_exporter
cp /VM_data/node_exporter/node_exporter.service /etc/systemd/system/
cp /VM_data/node_exporter/node_exporter.conf /etc/init/node_exporter.conf

systemctl daemon-reload
systemctl enable node_exporter
systemctl start node_exporter

